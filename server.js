// this will run the the main server and should hold the main configuration
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path:'./config.env'})
const app = require('./app')

const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD)

mongoose.connect(DB, {
    useNewUrlParser:true,
    useCreateIndex:true,
    useFindAndModify:false
}).then( () =>{console.log('DB connection is successed!')})




const port = process.env.PORT || 3000
app.listen(port, ()=> {
    console.log(`App is running on port ${port}`)
})
