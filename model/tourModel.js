
const mongoose = require('mongoose')
const tourScehma = new mongoose.Schema({
    name :{
        type: String,
        required: [true, 'A tour must have a name'],
        unique : true,
        trim:true
    },
    duration :{
        type: Number,
        requuired: [true, ' A tour must have a duration']
    },
    maxGroupSize : {
        type: Number,
        required : [true, 'A tour must have Group size']

    },
    difficulty :{
        type : String,
        required : [true, 'A tour must have difficulty']

    },
    ratingsAverage :{
        type : Number,
        default : 0

    },
    ratingsQuantity : {
        type : Number,
        default: 0
    },
   
    price:{
        type: Number,
        required: [true, 'A tour must have a price']
    },
    priceDiscount : Number,
    summary : {
        type : String,
        trim : true,
        required: [true, 'A tour must have summary']
    },
    description : {
        type : String , 
        trim : true
    },
    imageCover :{
        type :String, 
        required : [true, 'A tour must have a cover image']
    },
    images :[String],
    createAt : {
        type : Date, 
        default : Date.now(), 
        select: false
    }, 
    startDates : [Date]
})
const Tour = mongoose.model('Tour', tourScehma)

module.exports = Tour