
const express = require('express');
const morgan = require('morgan')

const app = express();

app.use(express.json())
if(process.env.NODE_ENV ==='development')
{

    app.use(morgan('dev'))
}
// In order to use userRouter and tourRouter files, we need to added in here 
const tourRouter = require('./Router/tourRouter')
const userRouter = require('./Router/userRouter')

app.use('/api/v1/tours',tourRouter)
app.use('/api/v1/users',userRouter)



module.exports = app;