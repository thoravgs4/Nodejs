
const express = require('express')
const userController = require('./../Controller/UserController')

const router = express.Router()


router
.route('/')
.get(userController.getAllUsers)
.post(userController.createUser)

router
.route('/:id')
.get(userController.getUser)
.patch(userController.updatUser)
.delete(userController.deleteUser)



module.exports = router





