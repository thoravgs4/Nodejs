

const express = require('express')

const tourController = require('./../Controller/TourController')
const router = express.Router();
//router.param('id',tourController.CheckID);

//Alias router middelware 
router
.route('/top-5-cheap')
.get(tourController.AliasTopTours,tourController.getAllTours)


router
.route('/')
.get(tourController.getAllTours)
.post(tourController.createTour)

router
.route('/:id')
.get(tourController.getTour)
.patch(tourController.updatTour)
.delete(tourController.deleteTour)







module.exports = router