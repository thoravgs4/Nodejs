

const fs = require('fs');
const http = require('http');
const url  = require('url');

// Non-blocking, asynchronous 
/*fs.readFile('./txt/start.txt', 'utf-8', (err,data )=>{
    console.log(data)
});

console.log('will read file!');
*/
//////////////////////////////
/////////// server ////////////

const server = http.createServer((req,res)=>{
    res.end('Hello from the server!')
})

server.listen(8000, '127.0.0.1', ()=>{
    console.log('Listening to request on port 8000')
})