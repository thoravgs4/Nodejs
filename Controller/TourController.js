
const express = require('express')
const Tour = require('./../model/tourModel')


exports.AliasTopTours = (req,res,next)=>{
   // req.query.limit = '5'
    //req.query.sort = 'ratingsAverage'
    req.query.fields = 'name,price,difficulty'
    next();


}

exports.getAllTours = async (req,res)=>{
    
  try {
        const queryObject = {...req.query};
        //const excludeField = ['page','limit','fields']
        
        //excludeField.forEach(el=> delete queryObject[el])
        // Advanced filtering 

        let queryStr = JSON.stringify(queryObject)
       
        // regular expression 
        queryStr =  queryStr.replace(/\b(gte|gt|lte|lt)\b/g, match => `$${match}`)
        queryStr = JSON.parse(queryStr)
        let  tQuery =  Tour.find(queryStr);
        //Sorting 
        
        if (req.query.sort){
            //const sortBody = req.query.sort.split(',').join(' ')
            //const sortBody = req.query.sort
            //tQuery= tQuery.sort({ field: 'asc', sortBody:-1 });
            tQuery =  tQuery.sort(req.query.sort)

        }else {
            tQuery = tQuery.sort('-createAt');
        }
    
            // Fields limiting
            if (req.query.fields){
             
                const fields = req.query.fields.split(',').join(' ')

                tQuery = tQuery.select(fields)
            }else
            { tQuery = tQuery.select('-__v')}

            // Pagination 
            const page = req.query.page *1 || 1
            const limit = req.query.limit * 1 || 100
            const skip = (page-1) *limit
            // page 1, 1-10, page 2, 11-20, page 3, 21-30

            // if the page is 2, the skip will be( 2-1)*10 = 10, 
            tQuery = tQuery.skip(skip).limit(limit)
            if(req.query.page){
                const numTours = await Tour.countDocuments();
                if (skip > numTours)throw new Error ('This page does not exist ')

            }

           const tours  = await tQuery;

            res.status(200).json({
                status: 'sucess',
                length: tours.length,
                tours:tours
                })

       
       
  } catch (error) {
      res.status(404).json({
          status: "failed",
          message: error
      })
    
}
  }
    

 exports.deleteTour = async (req,res)=>{
     try {
         const deltTour = await Tour.findByIdAndDelete(req.params.id)
       
         
     } catch (error) {
        res.status(404).json({
            status:'fail',
            message :error
        })
         
     }
    
   res.status(204).json({
       status:'success',
       message: 'This rout is not implemented'}) 
}

exports.getTour = async (req,res)=>{
    
     
    try {
        const tour =await  Tour.findById(req.params.id)
        // Tour.findOne({_id:req.param.id})
        res.status(200).json({
            status :"sucess",
            tours: tour
        })
        
    } catch (error) {
        res.status(404).json({
            status: 'failed',
            message : error
           }); 
        
    }
    }

exports.updatTour = async (req,res)=>{
    
    
    try {
        const tour = await Tour.findByIdAndUpdate(req.params.id,req.body, {new:true, runValidators:true}
                )
        res.status(200).json({
                status:'sucess', data: {
                message:'updating is completed',
                tour } })
   } catch (error) {
            res.status(404).json({
                status: 'Failed',
                message :error
            })
            
        }



}


exports.createTour= async (req,res)=>{

     
     try {
        const newTour = await Tour.create(req.body)
        res.status(200).json({
            status: 'sucess ',
             data :{
                tour : newTour
            }
        })
         
     } catch (error) {
         res.status(400).json({
             status:'failed',
             message : error
         })
         
     }

    
}
   
